<%--
  Created by IntelliJ IDEA.
  User: smh30
  Date: 10/01/2019
  Time: 2:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <title>Made it to JSP</title>
    </head>
    <body>
        <p>The path has arrived at the JSP: ${photosPath}</p>
        <table>
            <thead>
                <tr>
                    <th>Thumbnail</th>

                    <th><a href="ImageGalleryDisplay?sortColumn=filename&order=${nameSortToggle}ending"><img src='images/sort-${currNameSort}.png' alt='icon'></a>Filename</th>
                    <th><a href="ImageGalleryDisplay?sortColumn=filesize&order=${sizeSortToggle}ending"><img src='images/sort-${currSizeSort}.png' alt='icon'></a>File-size</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${filesList}" var="file">
                <tr>
                <td><a href="./Photos/${file.fullFile.name}">
                    <img src="./Photos/${file.thumbPath.name}" alt="${file.thumbDisplay}"></a>
                       </td>
                <td>${file.thumbDisplay}</td>
                <td>${file.fullfileSize}</td>
                </tr>
                </c:forEach>
        </table>
    </body>
</html>
