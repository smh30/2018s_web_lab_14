<%@ page import="java.util.Map" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: smh30
  Date: 10/01/2019
  Time: 11:46 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Server response</title>
    </head>
    <body>
        <h1>Server side response by JSP file</h1>
        <p>Thanks for your submission. The values sent to the server are as follows:</p>

        <% Map<String, String[]> surveyMap = null;
            surveyMap = request.getParameterMap();
            Iterator<Map.Entry<String, String[]>> i = surveyMap.entrySet().iterator();%>
        <table>
            <tbody>
                <%
                    while (i.hasNext()) {
                        Map.Entry<String, String[]> entry = i.next();
                        String key = entry.getKey();
                        String[] values = entry.getValue();

                        // so that it doesn't print the 'value' of the submit button
                        if (key.contains("submit") || key.contains("button")) {
                            continue;
                        }

                        int index = key.indexOf("[]");
                        if (index != -1) {
                            key = key.substring(0, index);
                        }%>
                <tr>
                    <td><%= key.toUpperCase() %>
                    </td>
                    <td>

                        <%
                            int j = 0;
                            for (String value : values) {
                        %><%= value %><%
                        // if there is more than one value and this isn't the last, add a comma
                        if (!(values.length == 1) && !(j == values.length - 1)) {
                    %>, <% }
                        j++;
                    }%>
                    </td>
                </tr>

                <% } %>
            </tbody>
        </table>


    </body>
</html>
