<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %><%--
  Created by IntelliJ IDEA.
  User: smh30
  Date: 10/01/2019
  Time: 10:46 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>exercise01-02</title>
    </head>
    <body>
        <h1>Exercise01 - 02</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Nam aliquam sem et tortor consequat id. Consequat nisl vel pretium lectus quam. Risus nullam
            eget felis eget nunc lobortis mattis. Id velit ut tortor pretium viverra suspendisse. Varius duis at
            consectetur lorem donec massa sapien faucibus et. Elit at imperdiet dui accumsan. Vestibulum mattis
            ullamcorper velit sed ullamcorper morbi tincidunt. Leo a diam sollicitudin tempor id. Volutpat diam ut
            venenatis tellus in metus. Dapibus ultrices in iaculis nunc sed augue.</p>

        <p>Maecenas sed enim ut sem viverra aliquet. Cras ornare arcu dui vivamus arcu felis bibendum. Pellentesque elit
            ullamcorper dignissim cras. Ipsum dolor sit amet consectetur. Adipiscing bibendum est ultricies integer quis
            auctor elit. Tempus urna et pharetra pharetra massa massa ultricies mi. Sagittis nisl rhoncus mattis rhoncus
            urna neque viverra justo nec. Nisi est sit amet facilisis magna etiam. Viverra nibh cras pulvinar mattis
            nunc sed. Feugiat vivamus at augue eget arcu dictum. Dignissim suspendisse in est ante. Turpis egestas
            pretium aenean pharetra magna ac placerat vestibulum. In nibh mauris cursus mattis molestie a. Diam
            sollicitudin tempor id eu nisl nunc mi ipsum. Sollicitudin aliquam ultrices sagittis orci a scelerisque
            purus. Ac placerat vestibulum lectus mauris. Arcu cursus vitae congue mauris rhoncus aenean vel elit. At
            risus viverra adipiscing at. Sed sed risus pretium quam vulputate.</p>

        <p> A cras semper auctor neque vitae tempus quam pellentesque nec. Ligula ullamcorper malesuada proin libero
            nunc consequat interdum. Vel orci porta non pulvinar neque laoreet. Pretium aenean pharetra magna ac
            placerat. Laoreet id donec ultrices tincidunt. Pulvinar mattis nunc sed blandit. Suspendisse interdum
            consectetur libero id. Euismod quis viverra nibh cras pulvinar mattis nunc sed blandit. Nec feugiat nisl
            pretium fusce. Aliquet nibh praesent tristique magna sit amet purus. Eu volutpat odio facilisis mauris sit.
            Eu tincidunt tortor aliquam nulla facilisi cras fermentum odio. Pellentesque id nibh tortor id aliquet
            lectus proin nibh.</p>

        <hr>

        <%@ include file="exercise02-date.jsp" %>

    </body>
</html>
