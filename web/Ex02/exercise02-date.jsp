<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.time.LocalDateTime" %><%--
  Created by IntelliJ IDEA.
  User: smh30
  Date: 10/01/2019
  Time: 11:20 AM
  To change this template use File | Settings | File Templates.
--%>
<%!
    // get the current datetime into a string variable using %!


    LocalDateTime time = LocalDateTime.now();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    String timeString = time.format(formatter);
%>

<p>Current Date and Time: <%=
// print the datetime string using %= var %
        timeString
%></p>
